"""
@title: pystatisticalanalysis/anova
@author: DerAndere
@created: 2022
SPDX-License-Identifier: MIT
Copyright 2022 DerAndere
@license: MIT
@about: pystatisticalanalysis anova
"""
__author__      = "DerAndere"
__date__        = 2022
__copyright__   = "Copyright 2022 DerAndere"
__license__     = "MIT"
__version__     = "0.0.1"
__status__      = "Prototype"
__maintainer__  = "DerAndere"
__contact__     = "DerAndere"

