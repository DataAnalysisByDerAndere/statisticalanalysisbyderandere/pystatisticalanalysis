"""
@title: pystatisticalanalysis/anova
@author: DerAndere
@created: 2022
SPDX-License-Identifier: MIT AND LicenseRef-PythonForDataScience AND LicenseRef-Bedre
Copyright 2022 DerAndere
@license: MIT
@about: pystatisticalanalysis ANOVA
"""

"""
Inspired by 
Renesh Bedre 2022. ANOVA using Python (with examples). Data science blog.
https://www.reneshbedre.com/blog/anova.html 
Copyright (c) 2022 Data science blog
SPDX-License-Identifier: LicenseRef-Bedre

and

Python for Data Science, LLC (2020). One-way ANOVA. Python for Data Science.
https://www.pythonfordatascience.org/anova-python/
Copyright by Python for Data Science, LLC 2018 - 2020
SPDX-License-Identifier: LicenseRef-PythonForDataScience

and 

Alex Maszanszki (2021). Two-way ANOVA with Python. 
https://www.kaggle.com/code/alexmaszanski/two-way-anova-with-python/notebook
Copyright 2021 Alex Maszanszki
SPDX-License-Identifier: Apache-2.0
"""

import pandas as pd
import scipy.stats as sstats
import statsmodels.api as sm
import statsmodels.formula.api as smf
import matplotlib.pyplot as plt

# load data file
df = pd.read_csv("https://reneshbedre.github.io/assets/posts/anova/onewayanova.txt", sep="\t")

# reshape the df dataframe as a stacked data frame suitable for statsmodels package
df_melt = pd.melt(df.reset_index(), id_vars=['index'], value_vars=['A', 'B', 'C', 'D'])

# replace column names
df_melt.columns = ['index', 'treatments', 'value']


# get ANOVA table as R like output

# Ordinary Least Squares (OLS) model
# Use other design formula for factorial ANOVA (e.g. two factors ANOVA, also called two-way ANOVA. Or ANCOVA if some independent variables are categorical and others continuous). See https://www.pythonfordatascience.org/factorial-anova-python/
fitted_model = smf.ols('value ~ C(treatments)', data=df_melt).fit()
anova_table = sm.stats.anova_lm(fitted_model, typ=2)
eta_squared(anova_table)
omega_squared(anova_table)
print(anova_table)


# perform multiple pairwise comparison (Tukey's HSD)
comp = sstats.multicomp.MultiComparison(df_melt['value'], df_melt['treatments'])
post_hoc_res = comp.tukeyhsd()
post_hoc_res.summary()

# QQ-plot

sm.qqplot(fitted_model.resid, line='q')  # line='q': Connect the first and third quantiles (Cleveland 1993. Visalizing Data, p. 31).
plt.xlabel("Theoretical Quantiles")
plt.ylabel("Residuals")
plt.show()

# Shapiro-Wilk test for normal distribution of residuals
w, pvalue = sstats.shapiro(model.resid)
print(w, pvalue)
# 0.9685019850730896 0.7229772806167603

# Bartlett's test for homogenity of variances.  
w, pvalue = sstats.bartlett(df['A'], df['B'], df['C'], df['D'])
print(w, pvalue)
# 5.687843565012841 0.1278253399753447


def eta_squared(aov):
    aov['eta_sq'] = 'NaN'
    aov['eta_sq'] = aov[:-1]['sum_sq']/sum(aov['sum_sq'])
    return aov

def omega_squared(aov):
    mse = aov['sum_sq'][-1]/aov['df'][-1]
    aov['omega_sq'] = 'NaN'
    aov['omega_sq'] = (aov[:-1]['sum_sq']-(aov[:-1]['df']*mse))/(sum(aov['sum_sq'])+mse)
    return aov
